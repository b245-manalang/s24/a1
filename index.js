// console.log('Hello World');

// Solution for Item 1
const getCube = Math.pow(2,3);
		
	let num = 2;
	let cube = `The cube of ${num} is ${getCube}`;
		console.log(cube);

// Solution for Item 2
let address = [1500, "Pier B St.", "Long Beach", "CA", 90813-2601];

let [houseNumber, street, city, state, zipCode] = address

fullAddress = `I live at ${houseNumber} ${street} ${city} ${state} ${zipCode}`;
console.log(fullAddress);


// Solution for Item 3
const animal = {
	name: "Willy",
	specie: "whale shark",
	weight: 75000,
	measurement: 65,
}

let {name, specie, weight, measurement} = animal;
console.log(`${name} is a ${specie}. He weighs at ${weight} pounds with a measurement of ${measurement} ft.`);

// Solution for Item 4
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));


// Solution for Item 5
let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);


// Solution for Item 6
class Dog {
  	constructor(name, age, breed){
	    this.name = name;
	    this.age = age;
	    this.breed = breed;
	}
}

let corgi = new Dog("Queenie", "4", "Corgi");
console.log(corgi);
